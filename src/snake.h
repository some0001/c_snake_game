enum Direction {UP, DOWN, LEFT, RIGHT};

struct Snake{
    char dir;
    int len;
    int body[100][2];
};

void snake_init(struct Snake *snake, int x_bound, int y_bound);
void snake_print(struct Snake *snake);
void snake_print_crash(struct Snake *snake);
int *snake_get_head_ptr(struct Snake *snake);
int *snake_get_next_pos(struct Snake *snake, enum Direction dir);
int snake_set_head(struct Snake *snake, int x, int y);
int snake_move(struct Snake *snake, enum Direction dir, int x_bound, int y_bound);
int snake_grow(struct Snake *snake, int x, int y);
