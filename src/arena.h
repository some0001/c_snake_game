struct Arena{
    char width;
    char height;
};

void arena_init(struct Arena *arena, int width, int height);
void arena_print(struct Arena *arena);
